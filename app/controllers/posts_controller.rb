class PostsController < ApplicationController

def index
end

def create
  @post = Post.create( user_params )
end

private

# Use strong_parameters for attribute whitelisting
# Be sure to update your create() and update() controller methods.

def user_params
  params.require(:post).permit(:picture)
end
end
